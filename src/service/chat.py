import asyncio

import src.config as config
from langchain.agents import LLMSingleActionAgent, AgentExecutor
from langchain.chains import LLMChain
from langchain.chat_models import ChatOpenAI
from langchain.memory import ConversationBufferWindowMemory
from src.ai.callbacks import AsyncCallbackHandler
from src.ai.output_parser import CustomOutputParser
from src.ai.template import CustomPromptTemplate, template
from src.ai.vector_db import DB_Tool
import langchain

tools = [DB_Tool]

langchain.debug = True

prompt = CustomPromptTemplate(template=template, tools=tools,
                              input_variables=["input", "intermediate_steps", "history"])

output_parser = CustomOutputParser()

tool_names = [tool.name for tool in tools]

streaming_llm = ChatOpenAI(model=config.llm_model, temperature=0.85, streaming=True)

llm_chain = LLMChain(llm=streaming_llm, prompt=prompt)
memory = ConversationBufferWindowMemory(k=3)

agent = LLMSingleActionAgent(
    llm_chain=llm_chain,
    output_parser=output_parser,
    stop=["\nObservation:"],
    allowed_tools=tool_names,
    memory=memory,
    return_intermediate_steps=False
)

agent_executor = AgentExecutor.from_agent_and_tools(agent=agent, tools=tools, verbose=False, memory=memory)


async def run_call(query: str, stream_it: AsyncCallbackHandler):
    # assign callback handler
    agent_executor.agent.llm_chain.llm.callbacks = [stream_it]

    # now query
    await agent_executor.acall(inputs={"input": query})


async def create_gen(query: str, stream_it: AsyncCallbackHandler):
    task = asyncio.create_task(run_call(query, stream_it))
    async for token in stream_it.aiter():
        yield token
    await task
