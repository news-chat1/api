import src.config as config
from langchain.agents import Tool
from langchain.chains import RetrievalQA
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import Milvus
from langchain_community.chat_models import ChatOpenAI

embeddings = OpenAIEmbeddings(model=config.embedding_model)

db_uri = f"https://{config.milvus_host}:{config.milvus_port}"
db = Milvus(embedding_function=embeddings, collection_name=config.milvus_collection,
            connection_args={"uri": db_uri, "user": config.milvus_user,
                             "password": config.milvus_password}, primary_field=config.milvus_primary_field,
            text_field=config.milvus_text_field, vector_field=config.milvus_vector_field)

query_llm = ChatOpenAI(model=config.llm_model, temperature=0.0)

base = RetrievalQA.from_chain_type(
    llm=query_llm,
    chain_type='stuff',
    retriever=db.as_retriever())

DB_Tool = Tool(
    name="Knowledge Base",
    func=base.run,
    description=(
        "Use this tool to get information to answer a question"
    ),
)
