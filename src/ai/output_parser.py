import re
from typing import Union

from langchain.agents import AgentOutputParser
from langchain.schema import AgentAction, AgentFinish


class CustomOutputParser(AgentOutputParser):
    def parse(self, llm_output: str) -> Union[AgentAction, AgentFinish]:
        # print("Out: ", llm_output, "\n\n\n")
        if 'Final Answer:' in llm_output:
            finish = AgentFinish(
                return_values={"output": llm_output.split("Final Answer:")[-1].strip()},
                log=llm_output,
            )
            return finish

        # print(llm_output, "\n\n\n")
        regex = r"Action\s*\d*\s*:(.*?)\nAction\s*\d*\s*Input\s*\d*\s*:[\s]*(.*)"
        matches = re.search(regex, llm_output, re.DOTALL)
        if not matches:
            return AgentFinish(return_values={"output": llm_output}, log=llm_output)
        action = matches.group(1).strip()
        action_input = matches.group(2).strip()

        return AgentAction(tool=action, tool_input=action_input.strip("").strip('"'), log=llm_output)
