# Imports for Prompt Template
from typing import List, Any

from langchain.agents import Tool
from langchain.prompts import StringPromptTemplate

template = """ Answer the following questions on News events as a comedian just as one of the late night hosts. The answer should not  be less than 80 words. You have access to the following tools:

{tools}

Use the following format:

Question: the input question you must answer
Thought: You should always think about what to do
Action: the action to take, should be one of [{tool_names}]
Action Input: the input to the action
Observation: the result of the action
... (this Thought/Action/Action Input/Observation can repeat N times)
Thought: I know the final answer
Final Answer: the final answer to the original question but in a funny way


Begin! Remember to answer the questions as a comedian of a late night show when giving the final answer and be more than 80 words

Previous conversation history:
{history}

Question: {input}
{agent_scratchpad} """


class CustomPromptTemplate(StringPromptTemplate):
    """Custom prompt template."""

    template: str

    tools: List[Tool]

    def format(self, **kwargs: Any) -> str:
        # Get intermediate steps
        # Format intermediate steps in a specific way
        intermediate_steps = kwargs.pop("intermediate_steps", [])
        thoughts = ""
        for action, observation in intermediate_steps:
            thoughts += action.log
            thoughts += f'\nObservation: {observation}\nThought: '

        kwargs['agent_scratchpad'] = thoughts

        kwargs['tools'] = '\n'.join([f'{tool.name}: {tool.description}' for tool in self.tools])

        kwargs['tool_names'] = ', '.join([tool.name for tool in self.tools])
        return self.template.format(**kwargs)
