import os

embedding_model = os.environ.get('EMBEDDING_MODEL', 'text-embedding-ada-002')
milvus_host = os.environ.get('MILVUS_HOST', '127.0.0.1')
milvus_port = os.environ.get('MILVUS_PORT', '19530')
milvus_user = os.environ.get('MILVUS_USER', "")
milvus_password = os.environ.get('MILVUS_PASSWORD', "")
milvus_collection = os.environ.get('MILVUS_COLLECTION', 'articles')
milvus_vector_field = os.environ.get('MILVUS_VECTOR_FIELD', 'embedding')
milvus_primary_field = os.environ.get('MILVUS_PRIMARY_FIELD', 'id')
milvus_text_field = os.environ.get('MILVUS_TEXT_FIELD', 'text')
llm_model = os.environ.get('LLM_MODEL', 'gpt-3.5-turbo-1106')
