import asyncio

from fastapi import APIRouter, Body
from fastapi.responses import StreamingResponse
from src.ai.callbacks import AsyncCallbackHandler
from src.schemas.schema import Query

from ..service.chat import agent_executor

router = APIRouter(tags=['LLM Chat'], prefix='/chat')


@router.post("/")
async def chat(query: Query = Body(...), ):
    stream_it = AsyncCallbackHandler()
    agent_executor.agent.llm_chain.llm.callbacks = [stream_it]
    asyncio.create_task(agent_executor.acall(inputs={"input": query.input}))
    return StreamingResponse(stream_it.aiter(), media_type="text/event-stream")
