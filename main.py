import os

from fastapi import FastAPI

from src.routers.api import router as chat_router

os.environ["OPENAI_API_KEY"] = "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"

api = FastAPI(
    title="Newschat Streaming API",
    description="This is a streaming API for the Newschat application",
    version="0.1",
    openapi_url="/openapi.json",
    docs_url="/",
    redoc_url=None,
    contact={
        "name": "Innocent Kithinji",
        "email": "innocent@ikithinji.com"
    }
)
api.include_router(chat_router)


@api.get("/health")
def health():
    return {"status": "ok 👍"}
