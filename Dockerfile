FROM --platform=linux/amd64 python:3.8.18
LABEL authors="innocentkithinji"

WORKDIR /app

ENV PORT 8000

COPY . /app

RUN pip install -r requirements.txt

RUN pip freeze > docker-requirements.txt

CMD exec uvicorn main:api --host 0.0.0.0 --port $PORT
